package org.hackfest.errorlineone;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import net.sf.json.*;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.dynamodb.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.amazonaws.services.dynamodb.model.BatchGetItemRequest;
import com.amazonaws.services.dynamodb.model.BatchGetItemResult;
import com.amazonaws.services.dynamodb.model.BatchResponse;
import com.amazonaws.services.dynamodb.model.Key;
import com.amazonaws.services.dynamodb.model.KeysAndAttributes;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;

public class PhishingProxy {
    private static final Logger logger = Logger.getLogger(PhishingProxy.class);

    static AmazonDynamoDBClient client;
    static AmazonSQS sqs;
    static String table1Name = "commoncrawl-hostip";
    static String queue_url;
    static String out_url;

    private static void createClient() throws IOException {
        AWSCredentials credentials = new PropertiesCredentials(
                PhishingProxy.class.getResourceAsStream("/AwsCredentials.properties"));

        client = new AmazonDynamoDBClient(credentials);
    }

    private static void retrieveMultipleItemsBatchGet(ArrayList<String> keystrs) {
        try {
            if (keystrs.size() == 0) {
                System.out.println("no keys to retrieve.");
                return;
            }

            Map<String, KeysAndAttributes> requestItems = new HashMap<String, KeysAndAttributes>();
            ArrayList<Key> keys = new ArrayList<Key>();

            for (int i = 0; i < keystrs.size(); i++) {
                keys.add(new Key().withHashKeyElement(new AttributeValue().withS(keystrs.get(i))));
            }

            requestItems.put(table1Name, 
                    new KeysAndAttributes()
                        .withKeys(keys));
           
            BatchGetItemResult result;
            BatchGetItemRequest batchGetItemRequest = new BatchGetItemRequest();
            do {
                System.out.println("Making the request.");
                                
                batchGetItemRequest.withRequestItems(requestItems);
                result = client.batchGetItem(batchGetItemRequest);
                                
                BatchResponse table1Results = result.getResponses().get(table1Name);
                if (table1Results != null){
                    System.out.println("Items in table " + table1Name);
                    for (Map<String,AttributeValue> item : table1Results.getItems() ) {
                        int count = countAttrs(item);
                        String verdict = count < 10 ? "normal" : "suspicious";

                        String msg = new String("phishingproxy\t" + item.get("hostip") + "\t" + Integer.toString(count) + "\t" + verdict);
                        System.out.println(msg);
                        logger.info("Sending message [" + msg + "]");
                        sqs.sendMessage(new SendMessageRequest()
                            .withQueueUrl(out_url)
                            .withMessageBody(msg));
                        logger.info("Sent a message to " + out_url);
                    }
                }
                
                // Check for unprocessed keys which could happen if you exceed provisioned
                // throughput or reach the limit on response size.
                for (Map.Entry<String,KeysAndAttributes> pair : result.getUnprocessedKeys().entrySet()) {
                    System.out.println("Unprocessed key pair: " + pair.getKey() + ", " + pair.getValue());
                }
                requestItems = result.getUnprocessedKeys();
            } while (result.getUnprocessedKeys().size() > 0);

                        
        }  catch (AmazonServiceException ase) {
            System.err.println("Failed to retrieve items.");
        }  
    }

    private static int countAttrs(Map<String, AttributeValue> attributeList) {
        int count = -1;
        for (Map.Entry<String, AttributeValue> item : attributeList.entrySet()) {
            count += 1;
            String attributeName = item.getKey();
            AttributeValue value = item.getValue();
            System.out.println(attributeName + " " +
                    (value.getS() == null ? "" : "S=[" + value.getS() + "]") +
                    (value.getN() == null ? "" : "N=[" + value.getN() + "]") +
                    (value.getSS() == null ? "" : "SS=[" + value.getSS() + "]") +
                    (value.getNS() == null ? "" : "NS=[" + value.getNS() + "] \n"));                   
        }

        return count;
    }

    public static void main(String[] args) throws Exception {
        createClient();

        sqs = new AmazonSQSClient(new PropertiesCredentials(
                PhishingProxy.class.getResourceAsStream("/AwsCredentials.properties")));

        GetQueueUrlRequest request = new GetQueueUrlRequest("record-in-susip");
        queue_url = sqs.getQueueUrl(request).getQueueUrl();
        request.setQueueName("record-view");
        out_url = sqs.getQueueUrl(request).getQueueUrl();

        while (true) {
            try {
                ArrayList<String> ips = new ArrayList<String>();

                // Get messages
                ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queue_url)
                    .withMaxNumberOfMessages(10);
                List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
                for (Message message : messages) {
                    //System.out.println("  Message");
                    //System.out.println("    MessageId:     " + message.getMessageId());
                    //System.out.println("    ReceiptHandle: " + message.getReceiptHandle());
                    //System.out.println("    MD5OfBody:     " + message.getMD5OfBody());
                    System.out.println("    Body:          " + message.getBody());
                    for (Map.Entry<String, String> entry : message.getAttributes().entrySet()) {
                        System.out.println("  Attribute");
                        System.out.println("    Name:  " + entry.getKey());
                        System.out.println("    Value: " + entry.getValue());
                    }

                    ips.add(message.getBody().split("\t")[2]);

                    // Delete a message
                    String messageRecieptHandle = messages.get(0).getReceiptHandle();
                    sqs.deleteMessage(new DeleteMessageRequest()
                        .withQueueUrl(queue_url)
                        .withReceiptHandle(messageRecieptHandle));
                }

                retrieveMultipleItemsBatchGet(ips);

            } catch (AmazonServiceException ase) {
                logger.error(ase);
            }

            Thread.sleep(2000);
        }
    }
}
