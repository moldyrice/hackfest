package org.hackfest.errorlineone;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Vector;

import org.apache.log4j.Logger;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import org.commoncrawl.protocol.shared.ArcFileItem;
import org.commoncrawl.protocol.shared.ArcFileHeaderItem;

import org.jsoup.Jsoup;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.dynamodb.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.amazonaws.services.dynamodb.model.PutItemRequest;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;

/**
 * Outputs all words contained within the displayed text of pages contained
 * within {@code ArcFileItem} objects.
 * 
 * @author Steve Salevan <steve.salevan@gmail.com>
 */
public class URLMapper extends MapReduceBase 
    implements Mapper<Text, ArcFileItem, Text, MapWritable> {
    private static final Logger logger = Logger.getLogger(URLMapper.class);

    static AmazonDynamoDBClient client;
    static AmazonSQSClient sqs;
    private String tablename;
    private String[] queue_names;
    private String[] queue_urls;

    public void configure(JobConf job) {
        super.configure(job);

        tablename = job.get("tablename_url");
        queue_names = job.getStrings("mapper.queue.names");
        queue_urls = new String[queue_names.length];
        String accesskey = job.get("accessKey");
        String secretkey = job.get("secretKey");

        AWSCredentials credentials = new BasicAWSCredentials(accesskey, secretkey);

        client = new AmazonDynamoDBClient(credentials);
        sqs = new AmazonSQSClient(credentials);
        for (int i = 0; i < queue_urls.length; i++) {
            GetQueueUrlRequest request = new GetQueueUrlRequest(queue_names[i]);
            queue_urls[i] = sqs.getQueueUrl(request).getQueueUrl();
        }
    }

    private void write_dynamodb(Text key, ArcFileItem value) {
        try {
            Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
            item.put("url", new AttributeValue().withS(key.toString()));
            item.put("hostIP", new AttributeValue().withS(value.getHostIP()));
            item.put("timestamp", new AttributeValue().withN(Long.toString(value.getTimestamp())));
            item.put("mimeType", new AttributeValue().withS(value.getMimeType()));
            item.put("recordLength", new AttributeValue().withN(Long.toString(value.getRecordLength())));

            ArrayList<ArcFileHeaderItem> headers = value.getHeaderItems();
            for (int i = 0; i < headers.size(); i++) {
                String header = headers.get(i).getItemKey();
                if (header.length() == 0)
                    item.put("return_code", new AttributeValue().withS(headers.get(i).getItemValue()));
                else
                    item.put(headers.get(i).getItemKey(), new AttributeValue().withS(headers.get(i).getItemValue()));
            }

            item.put("content", new AttributeValue().withS(value.getContent().getReadOnlyBytes().toString()));
            item.put("arcFileName", new AttributeValue().withS(value.getArcFileName()));
            item.put("arcFilePos", new AttributeValue().withS(Integer.toString(value.getArcFilePos())));

            PutItemRequest itemRequest = new PutItemRequest().withTableName(tablename).withItem(item);
            client.putItem(itemRequest);
            logger.debug("request put [" + item + "]");
            item.clear();

            // Send a message
            for (int i = 0; i < queue_urls.length; i++) {
                sqs.sendMessage(new SendMessageRequest()
                    .withQueueUrl(queue_urls[i])
                    .withMessageBody("crawler\t" + tablename + "\t" + key.toString()));
                logger.info("Sent a message to " + queue_names[i]);
            }
        } catch (AmazonServiceException ase) {
            logger.error(ase);
            logger.error("Failed to create item in " + tablename);
        }
    }

    public void map(Text key, ArcFileItem value, 
        OutputCollector<Text, MapWritable> output, Reporter reporter) throws IOException {
        try {
            if (!value.getMimeType().equals("text/html")) {
                return;  // Only parse text/html.
            }

            write_dynamodb(key, value);

            URL url = new URL(value.getUri());
            Text domain = new Text(url.getHost());
            LongWritable timestamp = new LongWritable(value.getTimestamp());
            Text nkey = new Text(value.getHostIP());
            MapWritable tpair = new MapWritable();

            tpair.put(domain, timestamp);

            output.collect(nkey, tpair);
        } catch (IOException e) {
            System.err.println(e);
            logger.info(Thread.currentThread().getStackTrace()[0].getMethodName());
            logger.error(e);
            reporter.getCounter("URLMapper.exception",
                e.getClass().getSimpleName()).increment(1);
            throw e;
        }
    }
}
