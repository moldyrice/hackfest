package org.hackfest.errorlineone;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.join.TupleWritable;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class URLReducer extends MapReduceBase
    implements Reducer<Text, MapWritable, Text, MapWritable> {
    private static final Logger logger = Logger.getLogger(URLReducer.class);

    public void reduce(Text key, Iterator<MapWritable> values,
            OutputCollector<Text, MapWritable> output, Reporter reporter)
        throws IOException {
        MapWritable domain_ts = new MapWritable();

        while (values.hasNext()) {
            MapWritable tuple = values.next();

            for (Map.Entry<Writable, Writable> entry : tuple.entrySet()) {
                Text domain = (Text) entry.getKey();
                LongWritable timestamp = (LongWritable) entry.getValue();

                if (!domain_ts.containsKey(domain) ||
                    (timestamp.compareTo(domain_ts.get(domain)) > 0)) {
                    logger.debug(domain + " timestamp " + timestamp + " is newer than " + domain_ts.get(domain));
                    domain_ts.put(domain, timestamp);
                }
            }
        }
        output.collect(key, domain_ts);
    }
}
