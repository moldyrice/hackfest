package org.hackfest.errorlineone;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import net.sf.json.*;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.dynamodb.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodb.model.AttributeAction;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.amazonaws.services.dynamodb.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodb.model.Key;
import com.amazonaws.services.dynamodb.model.PutItemRequest;
import com.amazonaws.services.dynamodb.model.ReturnValue;
import com.amazonaws.services.dynamodb.model.UpdateItemRequest;
import com.amazonaws.services.dynamodb.model.UpdateItemResult;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;

public class WebOfTrust {
    private static final Logger logger = Logger.getLogger(WebOfTrust.class);

    public enum WOTVerdict {
        NODATA ("no data"),
        PASS ("pass"),
        WARN ("dangerous");

        private final String message;

        WOTVerdict(String message) {
            this.message = message;
        }

        public String message() { return this.message; }
    }

    static String tablename = "maya-url";

    static JSONObject query(Set<String> hosts) throws Exception {
        String qstr = "";
        for (String h : hosts) {
            qstr += h + "/";
        }
        URL url = new URL("http://api.mywot.com/0.4/public_link_json?hosts=" + qstr);
        InputStream is = url.openStream();
        DataInputStream dis = new DataInputStream(new BufferedInputStream(is));
        BufferedReader d
                  = new BufferedReader(new InputStreamReader(dis));
        String s;
        StringBuffer sb = new StringBuffer();

        while ((s = d.readLine()) != null) {
            sb.append(s);
        }

        String json = sb.toString();  
        JSONObject jsonObject = JSONObject.fromObject( json );  

        return jsonObject;
    }

    public static void main(String[] args) throws Exception {
        PropertiesCredentials cred = new PropertiesCredentials(
                WebOfTrust.class.getResourceAsStream("/AwsCredentials.properties"));
        AmazonSQS sqs = new AmazonSQSClient(cred);
        AmazonDynamoDBClient client = new AmazonDynamoDBClient(cred);
        AmazonSNS sns = new AmazonSNSClient(cred);

        GetQueueUrlRequest request = new GetQueueUrlRequest("record-in-wot");
        String queue_url = sqs.getQueueUrl(request).getQueueUrl();
        String outarn = "arn:aws:sns:us-east-1:784333510955:wot";
        String sirenarn = "arn:aws:sns:us-east-1:784333510955:sirens"; 

        while (true) {
            try {
                HashMap<String, List<String> > urls = new HashMap<String, List<String> >();

                // Get messages
                ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queue_url)
                    .withMaxNumberOfMessages(10);
                List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
                for (Message message : messages) {
                    //System.out.println("  Message");
                    //System.out.println("    MessageId:     " + message.getMessageId());
                    //System.out.println("    ReceiptHandle: " + message.getReceiptHandle());
                    //System.out.println("    MD5OfBody:     " + message.getMD5OfBody());
                    //System.out.println("    Body:          " + message.getBody());
                    //for (Map.Entry<String, String> entry : message.getAttributes().entrySet()) {
                    //    System.out.println("  Attribute");
                    //    System.out.println("    Name:  " + entry.getKey());
                    //    System.out.println("    Value: " + entry.getValue());
                    //}

                    JSONObject content = JSONObject.fromObject(message.getBody()).getJSONObject("Message");
                    String urlstr = content.getString("url");
                    String host = new URL(urlstr).getHost();
                    if (urls.containsKey(host)) {
                        urls.get(host).add(urlstr);
                    } else {
                        ArrayList<String> ary = new ArrayList<String>();
                        ary.add(urlstr);
                        urls.put(host, ary);
                    }
                    System.out.println(urls);

                    // Delete a message
                    String messageRecieptHandle = messages.get(0).getReceiptHandle();
                    sqs.deleteMessage(new DeleteMessageRequest()
                        .withQueueUrl(queue_url)
                        .withReceiptHandle(messageRecieptHandle));
                }

                JSONObject jobj = query(urls.keySet());
                System.out.println(jobj);
                for (Map.Entry<String, List<String> > entry : urls.entrySet()) {
                    try {
                        String target = entry.getKey();
                        List<String> urlstrs = entry.getValue();
                        JSONObject jtarget = JSONObject.fromObject(jobj.get(target));

                        // 2: no data, 1: warning, 0: pass
                        WOTVerdict verdict = WOTVerdict.NODATA;
                        JSONObject info = new JSONObject();
                        if (jtarget.size() > 1) {
                            verdict = WOTVerdict.PASS;
                            info.put("target", target);
                            int reputation, confidence;
                            for (int j = 0; j < 5; j++) {
                                String key = Integer.toString(j); 
                                if (jtarget.has(key)) {
                                    JSONArray rt = JSONArray.fromObject(jtarget.get(key));
                                    info.put(key, rt);
                                    reputation = rt.getInt(0);
                                    confidence = rt.getInt(1);
                                    if ((reputation < 40) && (confidence >= 23))
                                        verdict = WOTVerdict.WARN;
                                }
                            }
                        }

                        JSONObject note = new JSONObject();
                        note.put("source", "wot");
                        note.put("verdict", verdict.message());
                        note.put("info", info);

                        String msg = note.toString();
                        System.out.println(msg);

                        for (String urlstr: urlstrs) {
                            try {
                                Map<String, AttributeValueUpdate> updateItems = new HashMap<String, AttributeValueUpdate>();
                                Key key = new Key().withHashKeyElement(new AttributeValue().withS(urlstr));
                                long ts = (new Date()).getTime() / 1000;

                                note.put("key", urlstr);
                                note.put("timestamp", ts);
                                
                                // Add verdict
                                updateItems.put("verdict-wot", 
                                  new AttributeValueUpdate()
                                    .withValue(new AttributeValue().withN(Integer.toString(verdict.ordinal()))));

                                updateItems.put("timestamp-wot",
                                    new AttributeValueUpdate()
                                        .withValue(new AttributeValue().withN(Long.toString(ts))));
                                updateItems.put("info-wot",
                                    new AttributeValueUpdate()
                                        .withValue(new AttributeValue().withS(info.toString())));
                                 
                                UpdateItemRequest updateItemRequest = new UpdateItemRequest()
                                  .withTableName(tablename)
                                  .withKey(key).withReturnValues(ReturnValue.UPDATED_NEW)
                                  .withAttributeUpdates(updateItems);
                                            
                                UpdateItemResult result = client.updateItem(updateItemRequest);

                                if (verdict == WOTVerdict.WARN) {
                                    sns.publish(new PublishRequest()
                                        .withTopicArn(sirenarn)
                                        .withMessage(msg));
                                }
                                sns.publish(new PublishRequest()
                                    .withTopicArn(outarn)
                                    .withMessage(msg));
                                //sqs.sendMessage(new SendMessageRequest()
                                //    .withQueueUrl(out_url)
                                //    .withMessageBody("wot\t" + msg));
                            } catch (AmazonServiceException ase) {
                                logger.error(ase);
                                logger.error("Failed to create item in " + tablename);
                            }
                        }
                    } catch (JSONException e) {
                        logger.error(e);
                    }
                }
            } catch (AmazonServiceException ase) {
                logger.error(ase);
            }

            Thread.sleep(2000);
        }
    }
}
