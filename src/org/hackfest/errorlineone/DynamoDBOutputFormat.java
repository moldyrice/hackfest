package org.hackfest.errorlineone;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.OutputCommitter;
import org.apache.hadoop.mapred.join.TupleWritable;
import org.apache.hadoop.mapred.lib.MultipleTextOutputFormat;
import org.apache.hadoop.mapred.lib.NullOutputFormat;
import org.apache.hadoop.mapred.OutputFormat;
import org.apache.hadoop.mapred.RecordWriter;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.util.Progressable;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.dynamodb.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.amazonaws.services.dynamodb.model.PutItemRequest;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;

public class DynamoDBOutputFormat<K, V> extends NullOutputFormat<K, V> {
    private static final Logger logger = Logger.getLogger(DynamoDBOutputFormat.class);

    protected static class DynamoDBRecordWriter<K, V> implements RecordWriter<K, V> {
        static AmazonDynamoDBClient client;
        static AmazonSQSClient sqs;
        private static final String utf8 = "UTF-8";

        private String tablename_hostip;
        private String tablename_url;
        private String[] queue_names;
        private String[] queue_urls;

        public DynamoDBRecordWriter(String accesskey, String secretkey, String tablehostip, String tableurl, String[] qnames) {
            tablename_hostip = tablehostip;
            tablename_url = tableurl;
            queue_names = qnames;
            queue_urls = new String[queue_names.length];
            //try {
                //AWSCredentials credentials = new PropertiesCredentials(
                //        DynamoDBRecordWriter.class.getResourceAsStream("AwsCredentials.properties"));
                //AmazonSQS sqs = new AmazonSQSClient(new PropertiesCredentials(
                //        SimpleQueueServiceSample.class.getResourceAsStream("AwsCredentials.properties")));
                AWSCredentials credentials = new BasicAWSCredentials(accesskey, secretkey);

                client = new AmazonDynamoDBClient(credentials);
                sqs = new AmazonSQSClient(credentials);
                for (int i = 0; i < queue_names.length; i++) {
                    GetQueueUrlRequest request = new GetQueueUrlRequest(queue_names[i]);
                    queue_urls[i] = sqs.getQueueUrl(request).getQueueUrl();
                }
            //} catch (IOException e) {
            //   System.err.println("failed to read credentials"); 
            //}
        }

        public synchronized void write(K key, V value) throws IOException {
            try {
                MapWritable val = (MapWritable) value;
                Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
                item.put("hostip", new AttributeValue().withS(key.toString()));
                logger.info("put hostip [" + key.toString() + "]");
                
                long min = Long.MAX_VALUE, max = Long.MIN_VALUE;

                for (Map.Entry<Writable, Writable> entry : val.entrySet()) {
                    String domain = ((Text) entry.getKey()).toString();
                    long timestamp = ((LongWritable) entry.getValue()).get();

                    item.put(domain, new AttributeValue().withN(Long.toString(timestamp)));
                    logger.info("\t[" + domain + "]\t" + Long.toString(timestamp));
                }  

                PutItemRequest itemRequest = new PutItemRequest().withTableName(tablename_hostip).withItem(item);
                client.putItem(itemRequest);
                item.clear();
                logger.info("request put");

                // Send a message
                for (int i = 0; i < queue_urls.length; i++) {
                    sqs.sendMessage(new SendMessageRequest()
                        .withQueueUrl(queue_urls[i])
                        .withMessageBody("crawler\t" + tablename_hostip + "\t" + key.toString()));
                    logger.info("Sent a message to " + queue_names[i]);
                }
            } catch (AmazonServiceException ase) {
                logger.error(ase);
                logger.error("Failed to create item in " + tablename_hostip);
            }
        }

        public synchronized void close(Reporter reporter) throws IOException {
            logger.info("close");
        }
    }

    public static void setTableNameHostip(JobConf job, String name) {
        job.set("tablename_hostip", name);
    }

    public static void setTableNameUrl(JobConf job, String name) {
        job.set("tablename_url", name);
    }

    public RecordWriter<K, V> getRecordWriter(FileSystem ignored, JobConf job,
        String name, Progressable progress) {

        String tablename_hostip = job.get("tablename_hostip");
        String tablename_url = job.get("tablename_url");
        String[] queue_names = job.getStrings("output.queue.names");
        String akey = job.get("accessKey");
        String skey = job.get("secretKey");

        return new DynamoDBRecordWriter<K, V>(akey, skey, tablename_hostip, tablename_url, queue_names);
    }

    //public void checkOutputSpecs(FileSystem ignored, JobConf job) {
    //}
}
