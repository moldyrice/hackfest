# vim: ts=4 sw=4 et :
# -*- coding: utf-8 -*-

import sys
import json
import time
import boto
#from boto.sqs.message import RawMessage
import boto.sns

import recparser

VIEW_QNAME = "record-view"
WOT_QNAME = "record-in-wot"
SUSIP_QNAME = "record-in-susip"
URL_TABLE = "maya-url"
URL_TOPIC_ARN = "arn:aws:sns:us-east-1:784333510955:record-in"

def main():
    if len(sys.argv) < 1:
        print "%s <file>" % sys.argv[0]
        return

    pagefile = sys.argv[1]
    #qnames = [VIEW_QNAME, WOT_QNAME, SUSIP_QNAME]

    with open(pagefile, "r") as f:
        ddb = boto.connect_dynamodb()
        sns = boto.sns.connect_to_region('us-east-1')
        #sqs = boto.connect_sqs()
        table = ddb.get_table(URL_TABLE)
        #queues = []
        #for name in qnames:
        #    queues.append(sqs.get_queue(name))

        for page in recparser.iter_pages(f):
            print page['U']
            item_data = {}
            for k in page:
                if k == 'raw' or k == 'U' or k == 'B':
                    continue
                tag = recparser.TAG2ATTR[k]
                val = str(page[k])
                if len(val):
                    item_data[tag] = val

            key = page['U']
            ts = page['it'] if hasattr(page, 'it') else time.time()
            try:
                item = table.new_item(hash_key = key, attrs = item_data)
                item.put()
            except boto.exception.DynamoDBResponseError, e:
                print e
                continue
            except UnicodeDecodeError, e:
                print e
                continue

            msg = {"source": "maya", "db": URL_TABLE, "url": key, 
                "timestamp": ts}
            sns.publish(URL_TOPIC_ARN, json.dumps(msg))
            #m = RawMessage()
            #m.set_body("maya\t" + URL_TABLE + "\t" + key)
            #print m.get_body()

            #for q in queues:
            #    status = q.write(m)
            #    retry = 0
            #    while (not status) and (retry < 5):
            #        time.sleep(5)
            #        status = q.write(m)

            #    if not status:
            #        print "failed to sent message to %s [%s]" % (q.id, key)

if __name__ == '__main__':
    main()
    
    sys.exit(0)
