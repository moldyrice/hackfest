#!/usr/bin/env python2.6
# vim: ts=4 sw=4 et :

import sys
from base64 import b64decode
from datetime import datetime


# output file will be written in this order
TAG_NAME  = 'L U H 64 B s pt ft rc ct tt tn rl us ex it ip DU pl'.split()
ATTR_NAME = ( 'line url header b64_encoded body size modify_time '
              'download_time response_code content_type true_file_type '
              'true_file_type_number redirecting_link source_name '
              'extra_information input_time ip download_url protocol' ).split()
TAG2ATTR = dict(zip(TAG_NAME, ATTR_NAME))
ATTR2TAG = dict(zip(ATTR_NAME, TAG_NAME))

class Tag(object):
    pass

for attr, tag in zip(ATTR_NAME, TAG_NAME):
    setattr(Tag, attr.upper(), tag)

Tag.TIMESTAMP_TAGS = [Tag.MODIFY_TIME, Tag.DOWNLOAD_TIME, Tag.INPUT_TIME, ]
Tag.INTEGER_TAGS   = [Tag.SIZE, Tag.PROTOCOL, ]
Tag.BOOLEAN_TAGS   = [Tag.B64_ENCODED, ]
Tag.MULTILINE      = [Tag.BODY, Tag.HEADER, ]


class Page(dict, object):
    def __init__(self, pairs=None):
        if pairs:
            super(Page, self).__init__(pairs)
        self.raw = True

    def __getattr__(self, name):
        if name in ATTR_NAME:
            if self.__contains__(ATTR2TAG[name]):
                return self.__getitem__(ATTR2TAG[name])
            return None
        else:
            if self.__contains__(name):
                return self.__getitem__(name)
            raise AttributeError()

    def __setattr__(self, name, value):
        if name in ATTR_NAME:
            self.__setitem__(ATTR2TAG[name], value)
        else:
            self.__setitem__(name, value)


# utilities ==================================================================

def is_bad_page(page):
    if page.size is not None:
        if int(page.size) == 0:
            return True
    if page.response_code is not None:
        if int(page.response_code) / 100 in [0, 1, 3, 4, 5]:
            return True
    if page.body is not None:
        content = page.body.strip()
        if page.b64_encoded:
            content = b64decode(content)
        if len(content.strip()) == 0:
            return True

    return False

def size_in_byte(text):
    return len(text.decode("iso-8859-1"))

# parse maya recfile to pages ================================================

def split_tag(tag):
    i = tag.find('>')
    return tag[1:i], tag[i+1:]

def iter_raw_pages(rec_line_iter):
    raw_page = []
    buffer = ''
    for line in rec_line_iter:
        if line.startswith('<'):
            if buffer:
                raw_page.append(split_tag(buffer))
                buffer = ''
            if line.startswith('<>'):
                if raw_page:
                    yield Page(raw_page)
                raw_page = []
                continue
        buffer += line
    raw_page.append(split_tag(buffer))
    yield Page(raw_page)

def cook(page, decode_body=True):
    if page.raw:
        # strip
        for tag in TAG_NAME:
            if tag in page and tag not in Tag.MULTILINE:
                page[tag] = page[tag].strip()

        # convert type
        for tag in Tag.INTEGER_TAGS + Tag.TIMESTAMP_TAGS:
            if tag in page:
                page[tag] = int(page[tag])
        for tag in Tag.BOOLEAN_TAGS:
            if tag in page:
                page[tag] = bool(int(page[tag]))

        # base64 decode body
        if decode_body and page.b64_encoded:
            page.body = b64decode(page.body)
            page.b64_encoded = False

        page.raw = False

    return page

def refrigerate(page):
    if not page.raw:
        # convert type
        for tag in Tag.INTEGER_TAGS + Tag.TIMESTAMP_TAGS:
            if tag in page:
                page[tag] = str(page[tag])
        for tag in Tag.BOOLEAN_TAGS:
            if tag in page:
                page[tag] = str(int(page[tag]))
        page.raw = True

    return page

def iter_pages(rec_line_iter, decode_body=True):
    for raw_page in iter_raw_pages(rec_line_iter):
        yield cook(raw_page, decode_body)

def iter_urls(rec_line_iter):
    for line in rec_line_iter:
        if line.startswith('<U>'):
            yield line[3:].strip()

# parse pages to maya recfile ================================================

def page2rec(page):
    result = '<>\n'
    for tag in TAG_NAME:
        if tag in page:
            if page[tag][-1:] != '\n':
                page[tag] += '\n'
            result += '<%s>%s' % (tag, page[tag])
            del page[tag]
    return result

def main(args):
    for page in iter_pages(sys.stdin):
        print page

if __name__ == '__main__':
    sys.exit(main(sys.argv))
