#!/bin/bash

set -e

outfile=$1
lastmod=""
dupfile=$(basename $outfile)

count=0
while true; do
    modtime=$(ls -l --time-style=+%s $outfile | cut -f 6 -d" ")
    if [ "$modtime" != "$lastmod" ]; then
        echo "$outfile modified"
        cp $outfile $dupfile.$count
        count=$((count + 1))
        echo $count
        #python sharkfin.py $dupfile
        lastmod=$modtime
    fi

    sleep 10
done
