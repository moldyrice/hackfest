import sys
import boto

URL_TABLE='maya-url'

def main(shark):
    with open(shark, "r") as f:
        ddb = boto.connect_dynamodb()

        table = ddb.get_table(URL_TABLE)

        for line in f:
            fields = line.strip().split("\t")
            url = fields[1]
            result = fields[5]
            score = fields[6]
            if result != -1:
                try:
                    item = table.get_item(hash_key = url, attributes_to_get = 
                        ['verdict-wot', 'info-wot', 'timestamp-wot'])
    
                    print item
                    if item['verdict-wot'] == 'dangerous' and score == 90:
                        print 'YOU FAIL!!! (false negative: %s)' % url
                        print 'WOT rating: %s' % item['info-wot']
                        print 'WOT rate-time: %s' % item['timestamp-wot']

                except boto.dynamodb.exceptions.DynamoDBKeyNotFoundError, e:
                    print "%s: %s" % (e, url)

if __name__ == '__main__':
    main(sys.argv[1])
