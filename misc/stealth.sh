#!/bin/bash

set -e

pagefile=$1
lastmod=""

while true; do
    modtime=$(ls -l --time-style=+%s $pagefile | cut -f 6 -d" ")
    if [ "$modtime" != "$lastmod" ]; then
        echo "$pagefile modified"
        python mantis.py $pagefile
        lastmod=$modtime
    fi

    sleep 10
done
