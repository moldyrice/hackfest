import sys
import time
import json
from urlparse import urlparse

import boto
from boto.sqs.message import RawMessage
import boto.sns

SUSIP_QNAME = "record-in-susip"
URL_TABLE = "maya-url"
HOSTIP_TABLE = "maya-hostip"
OUT_ARN = "arn:aws:sns:us-east-1:784333510955:susip"
SIREN_ARN = "arn:aws:sns:us-east-1:784333510955:sirens"

STRF_PATTERN = '%Y-%m-%dT%H:%M:%S.%fZ'
CHECK_RANGE = 3 * 60 * 60 *24 # 3 days
THRESHOLD = 8

def main(argv):
    ddb = boto.connect_dynamodb()
    sqs = boto.connect_sqs()
    sns = boto.sns.connect_to_region('us-east-1')

    url_tbl = ddb.get_table(URL_TABLE)
    hostip_tbl = ddb.get_table(HOSTIP_TABLE)

    qin = sqs.get_queue(SUSIP_QNAME)
    qin.set_message_class(RawMessage)

    while 1:
        qmsgs = qin.get_messages(num_messages = 10)
        keys = []

        for msg in qmsgs:
            m = json.loads(msg.get_body())['Message']
            content = json.loads(m)
            print content['url']
            keys.append(content['url'])
            msg.delete()

        batchlist = ddb.new_batch_list()
        batchlist.add_batch(url_tbl, keys, attributes_to_get = ['url', 'ip', 'input_time'])
        result = ddb.batch_get_item(batchlist)
        items = result['Responses'][URL_TABLE]['Items']

        ip_hosts = {}
        for item in items:
            ip = item['ip']
            url = urlparse(item['url'])
            hostname = url.hostname
            ts = item['input_time']

            if hostname:
                if ip in ip_hosts:
                    if hostname in ip_hosts[ip]:
                        if ts > ip_hosts[ip][hostname]:
                            ip_hosts[ip][hostname] = ts
                    else:
                        ip_hosts[ip][hostname] = ts
                else:
                    ip_hosts[ip] = {hostname: ts}

        for ip in ip_hosts:
            try:
                item = hostip_tbl.get_item(hash_key = ip)
                item.update(ip_hosts[ip])
            except boto.dynamodb.exceptions.DynamoDBKeyNotFoundError:
                item = hostip_tbl.new_item(hash_key = ip, attrs = ip_hosts[ip])

            item.put()

            count = 0
            tsmark = time.time() - CHECK_RANGE
            for key in item.keys():
                if key == 'hostip':
                    continue

                if item[key] > tsmark:
                    count += 1

            verdict = "pass" if count < THRESHOLD else "suspicious"
            m = {"source": "susip", "key": ip, "verdict": verdict,
                "timestamp": ts, "info": {"count": count}}
            msg = json.dumps(m)
            print m
            if count >= THRESHOLD:
                sns.publish(SIREN_ARN, msg)
            sns.publish(OUT_ARN, msg)
            #m = RawMessage()
            #m.set_body("susip\t%s\t%s\t%s" % (ip, count,
            #    "pass" if count < THRESHOLD else "suspicious"))
            #print m.get_body()
            #viewq.write(m)

if __name__ == '__main__':
    main(sys.argv)
