import sys
import time
import json
import urllib
from urlparse import urlparse

import boto
from boto.sqs.message import RawMessage
import boto.sns

WOT_QNAME = "record-in-wot"
URL_TABLE = "maya-url"
OUT_ARN = "arn:aws:sns:us-east-1:784333510955:wot"
SIREN_ARN = "arn:aws:sns:us-east-1:784333510955:sirens"

STRF_PATTERN = '%Y-%m-%dT%H:%M:%S.%fZ'
CHECK_RANGE = 3 * 60 * 60 *24 # 3 days
THRESHOLD = 8

def main(argv):
    ddb = boto.connect_dynamodb()
    sqs = boto.connect_sqs()
    sns = boto.sns.connect_to_region('us-east-1')

    url_tbl = ddb.get_table(URL_TABLE)

    qin = sqs.get_queue(WOT_QNAME)
    qin.set_message_class(RawMessage)

    while 1:
        qmsgs = qin.get_messages(num_messages = 10)
        hosts = {}

        for msg in qmsgs:
            m = json.loads(msg.get_body())['Message']
            content = json.loads(m)
            url = content['url']
            print content['url']
            hostname = urlparse(url).hostname
            if hostname in hosts:
                hosts[hostname].append(url)
            else:
                hosts[hostname] = [url]

        param = urllib.urlencode({'host': "/".join(hosts) + "/"})
        f = urllib.urlopen("http://api.mywot.com/0.4/public_link_json", param)
        jstr = f.read()
        wot = json.loads(jstr)

        print wot

        #for host, urls in hosts.items():
        #    for url in urls:
        #        try:
        #            item = url_tbl.get_item(hash_key = url)
        #            item['verdict-wot'] = verdict
        #            item.put()

        #            verdict = "pass" if count < THRESHOLD else "suspicious"
        #            m = {"source": "susip", "key": ip, "verdict": verdict,
        #                "timestamp": ts, "info": {"count": count}}
        #            msg = json.dumps(m)
        #            print m
        #            if count >= THRESHOLD:
        #                sns.publish(SIREN_ARN, msg)
        #            sns.publish(OUT_ARN, msg)

        #        except boto.dynamodb.exceptions.DynamoDBKeyNotFoundError, e:
        #            print e
        #            continue

if __name__ == '__main__':
    main(sys.argv)
